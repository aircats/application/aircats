# aircats

## Description
A simple Golang app serving a static site which is a clone of [HTTP cats](https://http.cat/)  
Automated deployment will compile and package the app and static assets into a zip file, then publish to a GitLab package repo.

## Local installation
1. Go must be [installed locally](https://golang.org/doc/install)
2. Clone main.go and static content (images of cats), then create a go module: `go mod init aircats`
3. Step 2 should generate a `go.mod` file
4. Run the application: `go run .`
5. The service should be listening on port 8080:
```
$ go run .
2021/06/18 13:30:14 Listening on 0.0.0.0:8080
```

## Local compiling
1. Compile using `go build -v -o "aircats"`
2. Once application is compiled, it can be run using:
```
$ ./httpcats
2021/06/18 13:30:14 Listening on 0.0.0.0:8080
```